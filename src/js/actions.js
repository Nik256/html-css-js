$(document).ready(function () {

    $("#li-fligths").click(function () {
        $("table").hide();
        $("#create-flight-button").show();
        $("#div-flights").show();
        $("#flights").show();
    });

    $("#li-members").click(function () {
        $("table").hide();
        $("#create-flight-button").hide();
        $("#div-members").show();
        $("#members").show();
    });

    $("#li-flightcrews").click(function () {
        $("table").hide();
        $("#create-flight-button").hide();
        $("#div-flightcrews").show();
        $("#flightcrews").show();
    });

    $("#li-dispatchers").click(function () {
        $("table").hide();
        $("#create-flight-button").hide();
        $("#div-dispatchers").show();
        $("#dispatchers").show();
    });

    var row = null;

    $("#create-flight-button").click(function () {
        clearInput();
        $(".modal").show();
        $("#dialog-form").show();
    });

    $("#flights td").click(clickRow);

    function clickRow() {
        row = $(this).parent();
        getData(row);
        $(".modal").show();
        $("#dialog-form").show();
        $("#delete-button").show();
    }

    function getData(row) {
        $("#code").val($(row.children().get(0)).text());
        $("#from").val($(row.children().get(1)).text());
        $("#to").val($(row.children().get(2)).text());
        $("#departure").val($(row.children().get(3)).text());
        $("#arrival").val($(row.children().get(4)).text());
        $("#airliner").val($(row.children().get(5)).text());
        $("#flightcrew").val($(row.children().get(6)).text());
        $("#status").val($(row.children().get(7)).text());
    }

    $("#delete-button").click(function () {
        if (row !== null) {
            row.remove();
        }
        row = null;
        $(".modal").hide();
    });

    $("#close-button").click(function () {
        $(".modal").hide();
        row = null;
    });

    $("#save-button").click(function () {
        if (checkInput()) {
            if (row !== null)
                saveData(row);
            else
                appendNewRow();
            $(".modal").hide();
            row = null;
            $(".new-td").click(clickRow);
        }
        else
            alert("Please fill out all required fields");
    });

    function checkInput() {
        return !($("#code").val() === "" ||
            $("#from").val() === "" ||
            $("#to").val() === "" ||
            $("#departure").val() === "" ||
            $("#arrival").val() === "" ||
            $("#airliner").val() === null ||
            $("#flightcrew").val() === "" ||
            $("#status").val() === "");
    }

    function saveData(row) {
        $(row.children().get(0)).text($("#code").val());
        $(row.children().get(1)).text($("#from").val());
        $(row.children().get(2)).text($("#to").val());
        $(row.children().get(3)).text($("#departure").val());
        $(row.children().get(4)).text($("#arrival").val());
        $(row.children().get(5)).text($("#airliner").val());
        $(row.children().get(6)).text($("#flightcrew").val());
        $(row.children().get(7)).text($("#status").val());
    }

    function appendNewRow() {
        $("#flights tbody").append("<tr>" +
            "<td  class='new-td'>" + $("#code").val() + "</td>" +
            "<td class='new-td'>" + $("#from").val() + "</td>" +
            "<td class='new-td'>" + $("#to").val() + "</td>" +
            "<td class='new-td'>" + $("#departure").val() + "</td>" +
            "<td class='new-td'>" + $("#arrival").val() + "</td>" +
            "<td class='new-td'>" + $("#airliner").val() + "</td>" +
            "<td class='new-td'>" + $("#flightcrew").val() + "</td>" +
            "<td class='new-td'>" + $("#status").val() + "</td>" + "</tr>");
    }

    function clearInput() {
        $("#code").val("");
        $("#from").val("");
        $("#to").val("");
        $("#departure").val("");
        $("#arrival").val("");
        $("#airliner").val("");
        $("#flightcrew").val("");
        $("#status").val("");
    }
});

